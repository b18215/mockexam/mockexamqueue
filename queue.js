let collection = [];


// Write the queue functions below.

function print() {
   return this.collection;
}

function enqueue(name) {
  collection.push(name);
  return collection;
}

function dequeue() {
  collection.shift();
  return collection;
}

function isEmpty() {
  if (collection.length === 0) {
    return true;
  } else {
    return false;
  }
}


function size() {
  return collection.length;
}


function  front() {
   
    if(this.isEmpty() === false) {
        return this.collection[0];
    }
  }


module.exports = {
	collection,
	print,
	enqueue,
	isEmpty,
	size,
    front,
	dequeue
	

};


